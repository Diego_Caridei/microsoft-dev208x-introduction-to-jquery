#Microsoft: DEV208x Introduction to jQuery#

![edx-microsoft-logo.jpg](https://bitbucket.org/repo/9zoaE8/images/1239304987-edx-microsoft-logo.jpg)


#Module 0

Welcome

Navigating edX

You, the self-paced learner

Outline, release dates and grading

Pre-course Survey


#Module 1 

Getting started with jQuery

Adding jQuery to a page

Using CSS selectors with jQuery

Navigating the DOM with jQuery

Getting started with DOM manipulation

jQuery tips and tricks

Quiz 3 of 3 possible points (3/3) 100%

Problem Scores:  1/1 1/1 1/1

Lab 10 of 10 possible points (10/10) 100%


#Module 2 

Event handlers

Modifying elements

Registering event handlers

Adding new elements

Animations

Removing, replacing and cloning

Quiz 6 of 6 possible points (6/6) 100%

Problem Scores:  3/3 3/3

Lab 10 of 10 possible points (10/10) 100%

Problem Scores:  10/10

#Module 3
Using promises

Web workers

Using deferred

JavaScript Object Notation

Calling the server

Final Exam 7 of 7 possible points (7/7) 100%

Lab 10 of 10 possible points (10/10) 100%

Practice Scores:  10/10